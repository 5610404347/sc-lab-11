package exception;

public class MyClass {
	public void methX() throws DataException{
		throw new DataException();
	}

	public void methY() throws FormatException {
		throw new FormatException();
	}
}
