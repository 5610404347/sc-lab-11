package hashmap;

import java.util.HashMap;

public class WordCounter {
	String message;
	HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}

	public void count(){
		String[] s = message.split(" ");
		for (int i = 0; i < s.length; i++) {
			if(!wordCount.containsKey(s[i])){
				wordCount.put(s[i], 1);
			}else{
				wordCount.replace(s[i], wordCount.get(s[i])+1);
			}
		}
	}
	
	public int hasWord(String word){
		int count = 0;
		if(wordCount.containsKey(word)){
			count = wordCount.get(word);
		}else{
			count = 0;
		}
		return count;
	}








}

